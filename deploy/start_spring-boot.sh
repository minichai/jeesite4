#!/usr/bin/env bash

##检查系统类型
export os_type=`uname`

##停止spring-boot函数
KillSpringBoot()
{
    pid=`ps -ef|grep spring-boot|grep java|awk '{print $2}'`
    echo "spring-boot list :$pid"
    if [ "$pid" = "" ]
    then
      echo "no spring-bot pid alive"
    else
      kill -9 $pid
    fi

}

## Kill 当前正在运行的spring-boot
KillSpringBoot

## Maven 编译
echo "-----------Maven 编译------------"
echo ${WORKSPACE}
cd ${WORKSPACE}/root
mvn clean install

## Maven 运行
echo "-----------Maven 运行------------"
cd ${WORKSPACE}/web
nohup mvn clean spring-boot:run -Dmaven.test.skip=true &

